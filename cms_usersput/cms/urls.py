from django.urls import path

from . import views


## le cambio de lugar porq el recurso me busca cualquier string
urlpatterns = [
    path('', views.index),
    path('loggedin/', views.loggedIn),
    path('logout/', views.logout_vista),
    path('<str:recurso>/', views.get_resource),


]
