from django.shortcuts import render

from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from .models import Contenido
from django.contrib.auth import logout
from django.shortcuts import redirect
from django.template import loader

formulario = """
<form action="" method="POST">
	Valor: <input type="text" name="valor">
	</br> <input type=submit values="enviar">
	
	
"""
def index(request):
	content_list = Contenido.objects.all()
	template = loader.get_template('cms/index.html')
	contexto={'content_list':content_list}
	return HttpResponse(template.render(contexto,request))

@csrf_exempt
def get_resource(request, recurso):
	if request.method in ["PUT", "POST"]:
		if request.method == "PUT":
			Valor = request.POST["valor"]
		else:
			Valor = request.body.decode('utf-8')

		try:
			contenido = Contenido.objects.get(clave=recurso)
			contenido.valor = Valor
			contenido.save()
		except Contenido.DoesNotExist:
			contenido = Contenido(clave=recurso, valor=Valor)
			contenido.save()
	##GET
	try:
		contenido = Contenido.objects.get(clave=recurso)
		respuesta = HttpResponse("El recurso pedido es: "+ contenido.clave + ". Su valor es: " + contenido.valor + ", su identificador es: "
								 + str(contenido.id))
	except Contenido.DoesNotExist:
		if request.user.is_authenticated:   ##solo deja escribir un recurso si estas autenticado
			respuesta =  HttpResponse(formulario)
		else:
			respuesta = HttpResponse("Tienes que darte de alta <a href=/login>autenticate</a>")
		return respuesta
	return respuesta

def loggedIn(request):   ##gestion de usuario
	if request.user.is_authenticated:     ##quiero saber si esta autenticado
		respuesta = "Bienvenido "+request.user.username+ " al CMS de saro"
	else:
		respuesta = "No estás autenticado, <a href=/admin>Autentícate</a>"

	return HttpResponse(respuesta)

def logout_vista(request):
	logout(request)
	return redirect('/cms/')

